const express = require("express");
const bodyParser = require("body-parser");
const pino = require("express-pino-logger")();
const cors = require("cors");
const path = require("path");

const app = express();
app.use(cors()); //Blocks browser from restricting any data

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(pino);
TWILIO_ACCOUNT_SID = "ACa057e5eb6f271130f9216806769fc3d0";
TWILIO_AUTH_TOKEN = "cdbb2238e22813233cbd1942ed8a620d";
TWILIO_PHONE_NUMBER = +12055962661;
if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
  const client = require("twilio")(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
  console.log("twilio1:", TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
}

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "client/build")));

  app.get("*", function (req, res) {
    res.sendFile(path.join(__dirname, "client/build", "index.html"));
  });
}

app.get("/api/greeting", (req, res) => {
  const name = req.query.name || "World";
  res.setHeader("Content-Type", "application/json");
  res.send(JSON.stringify({ greeting: `Hello ${name}!` }));
});

app.post("/api/messages", (req, res) => {
  res.header("Content-Type", "application/json");
  client.messages
    .create({
      from: process.env.TWILIO_PHONE_NUMBER,
      to: req.body.to,
      body: req.body.body,
    })
    .then(() => {
      res.send(JSON.stringify({ success: true }));
    })
    .catch((err) => {
      console.log(err);
      res.send(JSON.stringify({ success: false }));
    });
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, () =>
  console.log(`Express server is running on port: ${PORT}`)
);